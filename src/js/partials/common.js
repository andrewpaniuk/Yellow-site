$(document).ready(function() {

    $('.burger').on('click', function() {
        // $(this).toggleClass('active');
        $('.toggle-menu').slideToggle('400');
    });


    // only number
    $(".numberOnly").keypress(function(e) {
        if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
    });

    var screen = $(window).width();
    if (screen <= 768) {
        $('.slider').slick({
            autoplay: false,
            autoplaySpeed: 2000,
            arrows: true,
            prevArrow: $('.prev'),
            nextArrow: $('.next'),
        });
    };


    // $('.faq__answer').not(':first').hide();
    $('.faq__answer').hide();
    $('.faq__question').click(function() {

        var findArticle = $(this).next('.faq__answer');
        var findWrapper = $(this).closest('.faq__list');

        if (findArticle.is(':visible')) {
            findArticle.slideUp();
        } else {
            findWrapper.find('.faq__answer').slideUp();
            findArticle.slideDown();
        }
    });

    $('.l-slider__wrapper').slick({
            slidesToShow: 3,
            autoplay: false,
            autoplaySpeed: 2000,
            arrows: true,
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 3,
            nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
            prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>'
});
});
